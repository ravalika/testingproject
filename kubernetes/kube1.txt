building the image:
docker build .
docker build -t <tag name> .
to run image:
docker run -it <image name>
docker run -p 8080:8080 -p 50000:50000 <jenkins(image name)>
docker run -p <port no.> --name <name of container u want to give>
docker run -d -P --name <container name> <image name>
get inside the container in kubectl:
kubectl exec -it <pod name> bash
ex:kubectl exec -it <demo> -- /bin/bash


to get slave ip address inside the container:
awk 'END{print $1}' /etc/hosts

options of ls in linux:
https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.3.0/com.ibm.zos.v2r3.bpxa500/lscmd.htm
kubectl cp <pod name>:~/.ssh/id_rsa id_rsa


https://codelabs.developers.google.com/codelabs/cloud-hello-kubernetes/index.html#3


service(master) - jenkins2
service1(slave) - jenslave
kub-service(pslave) - kubslave

